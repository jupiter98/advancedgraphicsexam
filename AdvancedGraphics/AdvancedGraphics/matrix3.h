#pragma once

/* Matrix3 class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all the expected methods    */
class Euler;
class AxisAngle;
class Point3;
class Versor3;


class Matrix3
{
public:
	Versor3 firstVers, secondVers, thirdVers;
	/* fields */
	// DONE M-Fields: which fields to store? (also add a constructor taking these fields).

	// DONE M-Ide: this constructor construct the identity rotation
	Matrix3();

	Matrix3(Versor3 _vers1, Versor3 _vers2, Versor3 _vers3);

	// constructor that takes as input the coefficient (RAW-MAJOR order!)
	Matrix3(Scalar m00, Scalar m01, Scalar m02,
		Scalar m10, Scalar m11, Scalar m12,
		Scalar m20, Scalar m21, Scalar m22);

	Vector3 apply(Vector3  v) const;

	// Rotations can be applied to versors or vectors just as well
	Versor3 apply(Versor3 dir) const;

	Point3 apply(Point3 p) const;

	// syntactic sugar: "R( p )" as a synonim of "R.apply( p )"
	Versor3 operator() (Versor3 p);
	Point3  operator() (Point3  p);
	Vector3 operator() (Vector3 p);

	Versor3 axisX() const;  // DONE M-Ax a
	Versor3 axisY() const; // DONE M-Ax b
	Versor3 axisZ() const;// DONE M-Ax c

	// combine two rotations (r goes first!)
	Matrix3 operator * (Matrix3 r) const;
	// returns a rotation to look toward target, if you are in eye, and the up-vector is up
	static Matrix3 lookAt(Point3 eye, Point3 target, Versor3 up);
	// returns a rotation
	static Matrix3 toFrom(Versor3 to, Versor3 from);

	static Matrix3 toFrom(Vector3 to, Vector3 from);
	// conversions to this representation
	static Matrix3 from(Quaternion m);
	static Matrix3 from(Euler e);
	static Matrix3 from(AxisAngle e);

	// does this Matrix3 encode a rotation?
	bool isRot() const;

	// return a rotation matrix around an axis
	static Matrix3 rotationX(Scalar angleDeg);
	static Matrix3 rotationY(Scalar angleDeg);
	static Matrix3 rotationZ(Scalar angleDeg);

	void printf() const;

	Scalar det() const;
	Matrix3 inverse() const;

	void invert();

	// specific methods for Eulers...
	Matrix3 transposed() const;

	void transpose();
};


// interpolation of rotations
inline Matrix3 directLerp(const Matrix3& a, const Matrix3& b, Scalar t)
{
	// DONE M-directLerp: how to interpolate Matrix3s
	Matrix3 res;

	res.firstVers = normalize(lerp(a.firstVers.asVector(), b.firstVers.asVector(), t));
	res.secondVers = normalize(lerp(a.secondVers.asVector(), b.secondVers.asVector(), t));
	res.thirdVers = normalize(lerp(a.thirdVers.asVector(), b.thirdVers.asVector(), t));

	return res;
}

inline Matrix3 lerp(const Matrix3& a, const Matrix3& b, Scalar t) //the normal lerp needs to use the quaternion version 
{
	// DONE M-smartLerp: how to interpolate Matrix3s
	Quaternion qA = Quaternion::from(a);
	Quaternion qB = Quaternion::from(b);

	return Matrix3::from(lerp(qA, qB, t));
}




