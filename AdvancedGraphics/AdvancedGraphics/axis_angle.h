#pragma once

#include "versor3.h"

/* AxisAngle class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all expected methods    */

class Euler;
class Quaternion;
class Matrix3;
class Point3;


typedef double Scalar;

class AxisAngle
{
public:

	/* fields */
	// DONE A-Field: which fields to store? (also add a constructor taking these fields).
	Versor3 axis;
	Scalar angle;

	// DONE A-Ide: this constructor construct the identity rotation
	AxisAngle();

	// DONE A-FromPoint
	// returns a AxisAngle encoding a point
	AxisAngle(const Point3& p);
	AxisAngle(Versor3 d, Scalar k);

	Vector3 apply(Vector3  v) const;

	// Rotations can be applied to versors or vectors just as well
	Versor3 apply(Versor3 dir) const;

	Point3 apply(Point3 p) const;

	// syntactic sugar: "R( p )" as a synonym of "R.apply( p )"
	Versor3 operator() (Versor3 p);
	Point3  operator() (Point3  p);
	Vector3 operator() (Vector3 p);

	Versor3 axisX() const;  // DONE A-Ax a
	Versor3 axisY() const; // DONE A-Ax b
	Versor3 axisZ()const; // DONE A-Ax c

	// conjugate
	AxisAngle operator * (AxisAngle r) const;

	AxisAngle inverse() const;

	void invert();

	// returns a rotation to look toward target, if you are in eye, and the up-vector is up
	static AxisAngle lookAt(Point3 eye, Point3 target, Versor3 up);

	// returns a rotation
	static AxisAngle toFrom(Versor3 to, Versor3 from);

	static AxisAngle toFrom(Vector3 to, Vector3 from);

	// conversions to this representation
	static AxisAngle from(Matrix3 m);
	// DONE M2A
	static AxisAngle from(Euler e);
	static AxisAngle from(Quaternion q);// from https://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToAngle/index.htm


	// does this AxisAngle encode a point?
	bool isPoint() const;
	bool isRot() const;
	void printf() const;

	static AxisAngle rotationX(Scalar angleDeg);
	static AxisAngle rotationY(Scalar angleDeg);
	static AxisAngle rotationZ(Scalar angleDeg);
};

// interpolation or rotations
inline AxisAngle lerp(const AxisAngle& a, const AxisAngle& b, Scalar t)
{
	//// DONE A-Lerp: how to interpolate AxisAngles
	//// hints: shortest path! Also, consider them are 4D unit vectors.
	//Vector3 FirstVec = a.axis * a.angle;
	//Vector3 SecondVec = b.axis * b.angle;
	//
	//const Vector3 lerpedVect = lerp(FirstVec, SecondVec, t);

	return AxisAngle(slerp(a.axis, b.axis, t), (1 - t) * a.angle + t * b.angle);
}
