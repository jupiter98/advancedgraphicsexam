
#include <iostream>
#include "quaternion.h"
#include "vector3.h"
#include "versor3.h"
#include "point3.h"
#include "matrix3.h"
#include "euler.h"
#include "axis_angle.h"
#include <math.h>

typedef double Scalar;

Versor3::Versor3(Scalar _x, Scalar _y, Scalar _z) :x(_x), y(_y), z(_z) {}


Versor3 Versor3::right() { return Versor3(+1, 0, 0); } // aka EAST
Versor3 Versor3::left() { return Versor3(-1, 0, 0); } // aka WEST
Versor3 Versor3::up() { return Versor3(0, +1, 0); }
Versor3 Versor3::down() { return Versor3(0, -1, 0); }
Versor3 Versor3::forward() { return Versor3(0, 0, +1); } // aka NORTH
Versor3 Versor3::backward() { return Versor3(0, 0, -1); } // aka SOUTH

// access to the coordinates: to write them
Scalar& Versor3::operator[] (int i)
{
	if (i == 0) return x;
	if (i == 1) return y;
	if (i == 2) return z;
	//assert(0);
	return x;
}

// access to the coordinates: to read them
Scalar Versor3::operator[] (int i) const
{
	if (i == 0) return x;
	if (i == 1) return y;
	if (i == 2) return z;
	//assert(0);
	return x;
}

Vector3 Versor3::operator*(Scalar k) const
{
	return Vector3(k * x, k * y, k * z);
}

Versor3 Versor3::operator -() const
{
	return Versor3(-x, -y, -z);
}
Versor3 Versor3::operator +() const
{
	return Versor3(x, y, z);
}

Vector3 Versor3::asVector() const
{
	//return *this * 1;
	return Vector3(x, y, z);
}

void Versor3::printf() const
{
	std::cout << "[Versor]: " << "X:" << x << "," << "Y:" << y << "," << "Z:" << z << std::endl;
} // DONE Print



