#define _USE_MATH_DEFINES

#include <iostream>
#include "quaternion.h"
#include "vector3.h"
#include "versor3.h"
#include "point3.h"
#include "euler.h"
#include "axis_angle.h"
#include "matrix3.h"
#include <math.h>

// DONE E-Ide: this constructor construct the identity rotation
Euler::Euler() :x(0), y(0), z(0) {}

// DONE E-Constr
Euler::Euler(Scalar m00, Scalar m01, Scalar m02) : x(m00), y(m01), z(m02) {}


Vector3 Euler::apply(Vector3  v) const
{
	// TODO E-App: how to apply a rotation of this type?
	return Vector3();
}

// Rotations can be applied to versors or vectors just as well
Versor3 Euler::apply(Versor3 dir) const
{
	return apply(dir.asVector()).asVersor();
}

Point3 Euler::apply(Point3 p) const
{
	return apply(p.asVector()).asPoint();
}

// syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
Versor3 Euler::operator() (Versor3 p) { return apply(p); }
Point3  Euler::operator() (Point3  p) { return apply(p); }
Vector3 Euler::operator() (Vector3 p) { return apply(p); }

Versor3 Euler::axisX() const
{
	return Matrix3::from(*this).axisX();
} // DONE E-Ax a
Versor3 Euler::axisY() const
{
	return Matrix3::from(*this).axisY();
} // DONE E-Ax b
Versor3 Euler::axisZ() const
{
	return Matrix3::from(*this).axisZ();
} // DONE E-Ax c

// conjugate
Euler Euler::operator * (Euler b) const
{
	return Euler();
}

Euler Euler::inverse() const
{
	// DONE E-Inv a

	return Euler(-this->x,-this->y,-this->z);
}

void Euler::invert()
{
	Euler e = inverse();
	x = e.x;
	y = e.y;
	z = e.z;
	// DONE E-Inv b
}

// returns a rotation to look toward target, if you are in eye, and the up-vector is up
Euler Euler::lookAt(Point3 eye, Point3 target, Versor3 up)
{
	// TODO E-LookAt
	return Euler();
}

// returns a rotation
Euler Euler::toFrom(Versor3 to, Versor3 from)
{
	// TODO E-ToFrom
	return Euler();
}

Euler Euler::toFrom(Vector3 to, Vector3 from)
{
	return toFrom(normalize(to), normalize(from));
}

Euler Euler::from(Quaternion q)
{
	return from(Matrix3::from(q));
}

Euler Euler::from(Matrix3 m) //from slides
{
	Euler eul;

	Scalar thirdVersY = -m.thirdVers.y;

	if (thirdVersY <= -1.0)
	{
		eul.x = -M_PI_2;
	}
	else if (thirdVersY >= 1.0)
	{
		eul.x = M_PI_2;
	}
	else
	{
		eul.x = asin(thirdVersY);
	}

	if (abs(thirdVersY) > 1.0 - EPSILON)
	{
		eul.z = 0.0;
		eul.y = atan2(-m.firstVers.z, m.firstVers.x);
	}
	else
	{
		eul.y = atan2(m.thirdVers.x, m.thirdVers.z);
		eul.z = atan2(m.firstVers.y, m.secondVers.y);

	}
	eul.z = eul.z * 180 / M_PI;
	eul.x = eul.x * 180 / M_PI;
	eul.y = eul.y * 180 / M_PI;

	return eul;
}     // DONE M2E DONE


// does this Euler encode a rotation?
bool Euler::isRot() const
{
	// DONE E-isR
	return true;
}

// return a rotation matrix around an axis
Euler Euler::rotationX(Scalar angleDeg)
{
	return Euler(angleDeg, 0, 0);
}
// DONE E-Rx
Euler Euler::rotationY(Scalar angleDeg)
{
	return Euler(0, angleDeg, 0);
}// DONE E-Rx
Euler Euler::rotationZ(Scalar angleDeg)
{
	return Euler(0, 0, angleDeg);
}// DONE E-Rx

void Euler::printf() const
{
	std::cout << "[EULER]: " << "X:" << x << "," << "Y:" << y << "," << "Z:" << z << std::endl;
} // DONE Print
