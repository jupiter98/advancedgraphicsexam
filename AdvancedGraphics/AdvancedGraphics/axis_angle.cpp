
#include <iostream>
#define _USE_MATH_DEFINES

#include "quaternion.h"
#include "vector3.h"
#include "versor3.h"
#include "point3.h"
#include "matrix3.h"
#include "euler.h"
#include "axis_angle.h"
#include <math.h>

// AxisAngle class 
//this class is a candidate to store a rotation! 
//as such, it implements all expected methods    

// DONE A-Ide: this constructor construct the identity rotation
AxisAngle::AxisAngle() : axis(Versor3::up()), angle(0) {}

// DONE A-FromPoint
// returns a AxisAngle encoding a point
AxisAngle::AxisAngle(const Point3& p) : axis(normalize(p.asVector())), angle(0)
{

}

AxisAngle::AxisAngle(Versor3 d, Scalar k) : axis(d), angle(k) {}

Vector3 AxisAngle::apply(Vector3  v) const
{
	// DONE A-App: how to apply a rotation of this type?
	Vector3 resultingVect = Vector3(Quaternion::from(*this).apply(v));
	return resultingVect;
}

// Rotations can be applied to versors or vectors just as well
Versor3 AxisAngle::apply(Versor3 dir) const
{
	return apply(dir.asVector()).asVersor();
}

Point3 AxisAngle::apply(Point3 p) const
{
	return apply(p.asVector()).asPoint();
}

// syntactic sugar: "R( p )" as a synonym of "R.apply( p )"
Versor3 AxisAngle::operator() (Versor3 p) { return apply(p); }
Point3  AxisAngle::operator() (Point3  p) { return apply(p); }
Vector3 AxisAngle::operator() (Vector3 p) { return apply(p); }

Versor3 AxisAngle::axisX() const
{
	Versor3 temp = normalize(angle * Versor3::right());
	return temp;
	//alternative could be using the axis component of the matrix 
};  // DONE A-Ax a
Versor3 AxisAngle::axisY() const
{
	Versor3 temp = normalize(angle * Versor3::up());
	return temp;
}  // DONE A-Ax b
Versor3 AxisAngle::axisZ() const
{
	Versor3 temp = normalize(angle * Versor3::forward());
	return temp;
} // DONE A-Ax c

// conjugate
AxisAngle AxisAngle::operator * (AxisAngle r) const
{
	return AxisAngle();
}

AxisAngle AxisAngle::inverse() const
{
	// DONE A-Inv a
	return AxisAngle(axis, -angle);
}

void AxisAngle::invert()
{
	angle -= angle;
	// DONE A-Inv b
}

// returns a rotation to look toward target, if you are in eye, and the up-vector is up
AxisAngle AxisAngle::lookAt(Point3 eye, Point3 target, Versor3 up)
{
	// DONE A-LookAt
	AxisAngle temp = from(Matrix3::lookAt(eye, target, up)); //use matrix look at and from.
	return temp;
}

// returns a rotation
AxisAngle AxisAngle::toFrom(Versor3 to, Versor3 from)
{
	// DONE A-ToFrom
	Versor3 rotationAxis = normalize(cross(from, to));
	Scalar angleCos = dot(from, to);
	Scalar angleRad = acos(angleCos);

	return AxisAngle(rotationAxis, angleRad);
}

AxisAngle AxisAngle::toFrom(Vector3 to, Vector3 from)
{
	return toFrom(normalize(to), normalize(from));
}

// conversions to this representation
AxisAngle AxisAngle::from(Matrix3 m)
{
	Vector3 temp = Vector3((m.secondVers.z - m.thirdVers.y), (m.thirdVers.x - m.firstVers.z), (m.secondVers.x - m.firstVers.y));
	AxisAngle axisAngleTemp = AxisAngle(normalize(temp), acos((m.firstVers.x + m.secondVers.y + m.thirdVers.z - 1.0) / 2.0));
	return axisAngleTemp;
}
// DONE M2A
AxisAngle AxisAngle::from(Euler e)
{
	return from(Matrix3::from(e));
}

// DONE E2A

AxisAngle AxisAngle::from(Quaternion q)// from https://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToAngle/index.htm
{
	if (q.w > 1) normalize(q);

	Scalar angle = 2 * acos(q.w);
	Vector3 vectorTemp = Vector3(q.x, q.y, q.z);
	return AxisAngle(normalize(vectorTemp), 2.0 * acos(q.w));

}// DONE Q2A

// does this Angle encode a rotation?
bool AxisAngle::isRot() const
{
	return true;
}

// does this AxisAngle encode a point?
bool AxisAngle::isPoint() const
{
	// DONE A-isP
	if (angle <= EPSILON)
	{
		return true;
	}
	else
	{
		return false;
	}

}

void AxisAngle::printf() const
{
	std::cout << "[AXIS ANGLE]: \n";
	axis.printf();
	std::cout << "angle: " << angle;
} // DONE Print

AxisAngle AxisAngle::rotationX(Scalar angleDeg)
{
	angleDeg *= M_PI / 180;
	return AxisAngle(Versor3::right(), angleDeg);
}// DONE A-Rx
AxisAngle AxisAngle::rotationY(Scalar angleDeg)
{
	angleDeg *= M_PI / 180;
	return AxisAngle(Versor3::up(), angleDeg);
}// DONE A-Ry
AxisAngle AxisAngle::rotationZ(Scalar angleDeg)
{
	angleDeg *= M_PI / 180;
	return AxisAngle(Versor3::forward(), angleDeg);
}// DONE A-Rz

