#define _USE_MATH_DEFINES
#include <iostream>
#include "quaternion.h"
#include "versor3.h"
#include "point3.h"
#include "matrix3.h"
#include "euler.h"
#include "axis_angle.h"
#include "transform.h"
#include "vector3.h"
#include <math.h>


void check(bool condition, std::string descr)
{
	std::cout << "Test " << descr << ": "
		<< ((condition) ? "PASSED" : "FAILED")
		<< "\n";
}

void unitTestInverse()
{
	Transform t;
	t.translation = Vector3(2, 0, 0);
	t.scale = 3;

	Point3 p0, q, p1;

	q = t.apply(p0);

	Transform ti = t.inverse();
	p1 = ti.apply(q); // VOGLIO RIOTTENERE p

	check(p0.isEqual(p1), "INVERSE");
}


void unitTestCumlate()
{
	Transform t1, t2;
	t1.translation = Vector3(2, 0, 0);
	t1.scale = 4;
	t1.rotation = Quaternion(90, 0, 0);

	t2.translation = Vector3(4, -3, 1);
	t2.scale = 0.4;
	t2.rotation = Quaternion(0, 0, 90);

	Point3 p(3, 1, 3);
	Point3 q0 = t2.apply(t1.apply(p));
	Transform product = t2 * t1;
	Point3 q1 = product.apply(p);

	check(q0.isEqual(q1), "CUMULATE");
}

void unitTestToFrom() //unit test for printing
{
	Euler e(10, 0, 0);
	Euler resEuler = Euler::from(Matrix3::from(e));

	check(abs(e.x - resEuler.x) < EPSILON, "Euler Conversion X");
	check(abs(e.y - resEuler.y) < EPSILON, "Euler Conversion Y");
	check(abs(e.z - resEuler.z) < EPSILON, "Euler Conversion Z");

	Matrix3 m = Matrix3::rotationY(30);
	Matrix3 resMat = Matrix3::from(Euler::from(m));

	check(resMat.firstVers.asVector().isEqual(m.firstVers.asVector()), "Matrix3 Conversion X");
	check(resMat.secondVers.asVector().isEqual(m.secondVers.asVector()), "Matrix3 Conversion Y");
	check(resMat.thirdVers.asVector().isEqual(m.thirdVers.asVector()), "Matrix3 Conversion Z");

	AxisAngle axis = AxisAngle(Versor3::right(), 1.57082);
	AxisAngle as = AxisAngle::from(Quaternion::from(axis));
	check(axis.axis.asVector().isEqual(as.axis.asVector()), "aXIS ANGLE from quaternion AXIS");
	check(abs(axis.angle - as.angle) < EPSILON, "axis angle from quaternion angle");

	Quaternion quat(0.7071, 0, 0, 0.7071);
	Quaternion from = Quaternion::from(AxisAngle::from(quat));
	check(abs(quat.x - from.x) < EPSILON, "quaternion from axisAngle scalar x");
	check(abs(quat.y - from.y) < EPSILON, "quaternion from axisAngle scalar z");
	check(abs(quat.z - from.z) < EPSILON, "quaternion from axisAngle scalar y");
	check(abs(quat.w - from.w) < EPSILON, "quaternion from axisAnglew scalar w");
}

void unitTestFromTwo()
{
	Vector3 from = Vector3(Versor3::up().asVector());
	Vector3 to = Vector3(Versor3::left().asVector());

	Matrix3 MatRot = Matrix3::toFrom(to, from);
	Vector3 Mat = MatRot.apply(from);

	Quaternion QuatRot = Quaternion::toFrom(to, from);
	Vector3 Quat = QuatRot.apply(from);
	AxisAngle AxisAngleRot = AxisAngle::toFrom(to, from);
	Vector3 axisAngle = AxisAngleRot.apply(from);


	check(axisAngle.isEqual(Mat), "FROM AxisAngle TO Mat");
	check(axisAngle.isEqual(Quat), "FROM AxisAngle TO Quat");
	check(Mat.isEqual(Quat), "FROM Mat TO Quat");

	check(Mat.isEqual(to), "Mat is equals to TO");
	check(Quat.isEqual(to), "Quat is equals to TO");
	check(axisAngle.isEqual(to), "AxisAngle is equals to TO");
}

int main()
{
	unitTestToFrom();
	unitTestFromTwo();
	unitTestInverse();
	unitTestCumlate();

	return 0;
}