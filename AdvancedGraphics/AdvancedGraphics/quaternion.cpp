
#include <iostream>
#include "quaternion.h"
#include "vector3.h"
#include "versor3.h"
#include "point3.h"
#include "matrix3.h"
#include "euler.h"
#include "axis_angle.h"
#include <math.h>
/* Quaternion class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all expected methods    */


Quaternion::Quaternion(Scalar _x, Scalar _y, Scalar _z, Scalar _w) : x(_x), y(_y), z(_z), w(_w)
{
	// DONE Q-Constr
}

Quaternion::Quaternion(Scalar _x, Scalar _y, Scalar _z) : x(_x), y(_y), z(_z), w(0) {}

// DONE Q-Ide: this constructor constructs the identity rotation
Quaternion::Quaternion() : x(0), y(0), z(0), w(1) {}

// DONE Q-FromPoint
// returns a quaternion encoding a point
Quaternion::Quaternion(const Point3& p) : x(p.x), y(p.y), z(p.z), w(0)
{

}
Quaternion::Quaternion(const Versor3& v) : x(v.x), y(v.y), z(v.z), w(0)
{

}
Quaternion::Quaternion(const Vector3& v) : x(v.x), y(v.y), z(v.z), w(0)
{

}

Vector3 Quaternion::apply(Vector3  v) const
{
	// DONE Q-App: how to apply a rotation of this type?
	Quaternion quatVec = Quaternion(v.asPoint()); //create a pure quaternion 
	Quaternion newQuat = *this * quatVec * this->conjugated(); // re - multiply it with q and post - multiply it with the conjugate q *
	return Vector3(newQuat.x, newQuat.y, newQuat.z);

}

// Rotations can be applied to versors or vectors just as well DONE
Versor3 Quaternion::apply(Versor3 dir) const
{
	return apply(dir.asVector()).asVersor();
}

Point3 Quaternion::apply(Point3 p) const
{
	return apply(p.asVector()).asPoint();
}

// syntactic sugar: "R( p )" as a synonym of "R.apply( p )"
Versor3 Quaternion::operator() (Versor3 p) { return apply(p); }
Point3  Quaternion::operator() (Point3  p) { return apply(p); }
Vector3 Quaternion::operator() (Vector3 p) { return apply(p); }

Versor3 Quaternion::axisX() const
{
	return inverse().apply(Versor3::right());
}

Versor3 Quaternion::axisY() const
{
	return inverse().apply(Versor3::up());
}

Versor3 Quaternion::axisZ() const
{
	return inverse().apply(Versor3::forward());
}

Quaternion Quaternion::operator*(Quaternion r) const //from lesson. DONE
{
	Vector3 firstVector = Vector3(x, y, z);
	Vector3 secondVector = Vector3(r.x, r.y, r.z);
	Scalar secondImaginary = r.w;

	Scalar realValue = (w * secondImaginary) - dot(firstVector, secondVector);
	Vector3 immaginaryValue = (firstVector * secondImaginary) + (secondVector * w) + cross(firstVector, secondVector);

	return Quaternion(immaginaryValue.x, immaginaryValue.y, immaginaryValue.z, realValue);
}

Quaternion Quaternion::operator*(Scalar k)const
{
	return Quaternion(k * x, k * y, k * z);
}

// Quaternion sum
Quaternion Quaternion::operator+(const Quaternion& b) const
{
	return Quaternion(x + b.x, y + b.y, z + b.z, w + b.w);
}

Quaternion Quaternion::operator-() const
{
	this->x * -1;
	this->y * -1;
	this->z * -1;
	this->w * -1;
	return *this;
}

Quaternion Quaternion::inverse() const
{
	// DONE Q-Inv a		   
	return conjugated();
}

void Quaternion::invert()
{
	// DONE Q-Inv b
	conjugate();
}

// specific methods for quaternions...
Quaternion Quaternion::conjugated() const //DONE
{
	return Quaternion(-x, -y, -z, w);
}

void Quaternion::conjugate()
{
	// DONE Q-Conj b
	x *= -1;
	y *= -1;
	z *= -1;
}

// returns a rotation to look toward target, if you are in eye, and the up-vector is up
Quaternion Quaternion::lookAt(Point3 eye, Point3 target, Versor3 up)
{
	// DONE Q-LookAt
	Versor3 lookDirection = normalize(target - eye); //get versor of direction
	Versor3 rotationAxis = normalize(cross(up, lookDirection)); //get rotation versor 

	Scalar viewAngle = acos(dot(Versor3::forward(), lookDirection)); //calculate the angle between the forward and look direction 

	return from(AxisAngle(rotationAxis, viewAngle)); //once we have the axis angle we can convert it with the from method

	//return from(
}

// returns a rotation
Quaternion Quaternion::toFrom(Versor3 to, Versor3 from)
{
#pragma region NewImplementation

	//DONE Q-ToFrom
	/*Quaternion quatTemp;

	Vector3 axis = cross(from, to);

	quatTemp.x = axis.x;
	quatTemp.y = axis.y;
	quatTemp.z = axis.z;

	quatTemp.w = dot(from, to);
	normalize(quatTemp);
	return quatTemp;*/
#pragma endregion
	return Quaternion::from(AxisAngle::toFrom(to, from));
}

Quaternion Quaternion::toFrom(Vector3 to, Vector3 from)
{
	return toFrom(normalize(to), normalize(from));
}

// conversions to this representation
Quaternion Quaternion::from(const Matrix3& m)
{
	return from(AxisAngle::from(m));
}// DONE M2Q
Quaternion Quaternion::from(const Euler& e) // https://www.wikiwand.com/en/Conversion_between_quaternions_and_Euler_angles
{
	// Abbreviations for the various angular functions
	double cYaw = cos(e.z * 0.5);//yaw
	double sYaw = sin(e.z * 0.5);
	double cPitch = cos(e.y * 0.5);//pitch
	double sPitch = sin(e.y * 0.5);
	double cRoll = cos(e.x * 0.5);//roll
	double sRoll = sin(e.x * 0.5);

	Quaternion q;
	q.w = cRoll * cPitch * cYaw + sRoll * sPitch * sYaw;
	q.x = sRoll * cPitch * cYaw - cRoll * sPitch * sYaw;
	q.y = cRoll * sPitch * cYaw + sRoll * cPitch * sYaw;
	q.z = cRoll * cPitch * sYaw - sRoll * sPitch * cYaw;

	return q;

}// DONE E2Q
Quaternion Quaternion::from(const AxisAngle& e)
{
	Scalar hSin = sin(e.angle / 2.0);
	return Quaternion((e.axis * hSin).x, (e.axis * hSin).y, (e.axis * hSin).z, cos(e.angle / 2.0));
}; // DONE A2Q

// does this quaternion encode a rotation?
bool  Quaternion::isRot() const
{
	// DONE Q-isR
	if (w == 0)
	{
		return false;
	}
	if ((x * x + y * y + z * z + w * w) - 1 <= EPSILON2)
	{
		return true;
	}
	else
	{
		return false;
	}
}


// does this quaternion encode a point?
bool Quaternion::isPoint() const
{
	if (w <= EPSILON)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void  Quaternion::printf() const
{
	std::cout << "Quaternion: " << "X:" << x << "," << "Y:" << y << "," << "Z:" << z << "," << "W:" << w << std::endl;
} // DONE Print

Quaternion Quaternion::rotationX(Scalar angleDeg)
{
	return from(AxisAngle::rotationX(angleDeg));
}
// DONE M-Rx
Quaternion Quaternion::rotationY(Scalar angleDeg)
{
	return from(AxisAngle::rotationY(angleDeg));
}// DONE M-Ry
Quaternion Quaternion::rotationZ(Scalar angleDeg)
{
	return from(AxisAngle::rotationZ(angleDeg));
} // DONE M-Rz


