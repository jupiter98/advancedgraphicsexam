#pragma once
#define _USE_MATH_DEFINES
#include <iostream>
#include "axis_angle.h"
#include "quaternion.h"
#include "vector3.h"
#include "point3.h"
#include "matrix3.h"
#include "euler.h"
#include <math.h>

/* Matrix3 class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all the expected methods    */

// DONE M-Ide: this constructor construct the identity rotation
Matrix3::Matrix3() : firstVers(Versor3::right()), secondVers(Versor3::up()), thirdVers(Versor3::forward()) {}

Matrix3::Matrix3(Versor3 _vers1, Versor3 _vers2, Versor3 _vers3) : firstVers(_vers1), secondVers(_vers2), thirdVers(_vers3) {}

// constructor that takes as input the coefficient (RAW-MAJOR order!)
Matrix3::Matrix3(Scalar m00, Scalar m01, Scalar m02,
	Scalar m10, Scalar m11, Scalar m12,
	Scalar m20, Scalar m21, Scalar m22) : firstVers(normalize(Vector3(m00, m10, m20))),
	secondVers(normalize(Vector3(m01, m11, m21))),
	thirdVers(normalize(Vector3(m02, m12, m22)))
{
	// DONE M-Constr
}

Vector3 Matrix3::apply(Vector3  v) const
{
	// DONE M-App: how to apply a rotation of this type?
	return Vector3(v.x * firstVers.x + v.y * secondVers.x + v.z * thirdVers.x,
		v.x * firstVers.y + v.y * secondVers.y + v.z * thirdVers.y,
		v.x * firstVers.z + v.y * secondVers.z + v.z * thirdVers.z);
}

// Rotations can be applied to versors or vectors just as well
Versor3 Matrix3::apply(Versor3 dir) const
{
	return apply(dir.asVector()).asVersor();
}

Point3 Matrix3::apply(Point3 p) const
{
	return apply(p.asVector()).asPoint();
}

// syntactic sugar: "R( p )" as a synonim of "R.apply( p )"
Versor3 Matrix3::operator() (Versor3 p) { return apply(p); }
Point3  Matrix3::operator() (Point3  p) { return apply(p); }
Vector3 Matrix3::operator() (Vector3 p) { return apply(p); }

Versor3 Matrix3::axisX() const
{
	return firstVers;
};  // DONE M-Ax a
Versor3 Matrix3::axisY() const
{
	return secondVers;
};  // DONE M-Ax b
Versor3 Matrix3::axisZ() const
{
	return thirdVers;
};  // DONE M-Ax c

// combine two rotations (r goes first!)
Matrix3 Matrix3::operator * (Matrix3 m) const
{
	Matrix3 finalMat;

	finalMat.firstVers.x = firstVers.x * m.firstVers.x + secondVers.x * m.firstVers.y + thirdVers.x * m.firstVers.z;
	finalMat.secondVers.x = firstVers.x * m.secondVers.x + secondVers.x * m.secondVers.y + thirdVers.x * m.secondVers.z;
	finalMat.thirdVers.x = firstVers.x * m.thirdVers.x + secondVers.x * m.thirdVers.y + thirdVers.x * m.thirdVers.z;

	finalMat.firstVers.y = firstVers.y * m.firstVers.x + secondVers.y * m.firstVers.y + thirdVers.y * m.firstVers.z;
	finalMat.secondVers.y = firstVers.y * m.secondVers.x + secondVers.y * m.secondVers.y + thirdVers.y * m.secondVers.z;
	finalMat.thirdVers.y = firstVers.y * m.thirdVers.x + secondVers.y * m.thirdVers.y + thirdVers.y * m.thirdVers.z;

	finalMat.firstVers.z = firstVers.z * m.firstVers.z + secondVers.z * m.firstVers.y + thirdVers.z * m.firstVers.z;
	finalMat.secondVers.z = firstVers.z * m.secondVers.z + secondVers.z * m.secondVers.y + thirdVers.z * m.secondVers.z;
	finalMat.thirdVers.z = firstVers.z * m.thirdVers.z + secondVers.z * m.thirdVers.y + thirdVers.z * m.thirdVers.z;

	return finalMat;
}//DONE
// returns a rotation to look toward target, if you are in eye, and the up-vector is up

Matrix3 Matrix3::lookAt(Point3 eye, Point3 target, Versor3 up)
{
	Matrix3 rotationMat;
	Versor3 directionVector = normalize(target - eye);
	rotationMat.firstVers = normalize(cross(up, rotationMat.thirdVers));
	rotationMat.secondVers = normalize(cross(rotationMat.thirdVers, rotationMat.firstVers));
	rotationMat.thirdVers = directionVector;
	// DONE M-LookAt
	return rotationMat;
}

// returns a rotation
Matrix3 Matrix3::toFrom(Versor3 to, Versor3 from) //https://www.programcreek.com/cpp/?CodeExample=look+at //https://stackoverflow.com/questions/53143175/writing-a-lookat-function
{
	// DONE M-ToFrom 
	/*Vector3 crossedAxis = cross(to, from);
	const Scalar cosA = dot(to, from);
	const Scalar k = 1.0f / (1.0f + cosA);

	Matrix3 result(
		(crossedAxis.x * crossedAxis.x * k) + cosA,
		(crossedAxis.y * crossedAxis.x * k) - crossedAxis.z,
		(crossedAxis.z * crossedAxis.x * k) + crossedAxis.y,
		(crossedAxis.x * crossedAxis.y * k) + crossedAxis.z,
		(crossedAxis.y * crossedAxis.y * k) + cosA,
		(crossedAxis.z * crossedAxis.y * k) - crossedAxis.x,
		(crossedAxis.x * crossedAxis.z * k) - crossedAxis.y,
		(crossedAxis.y * crossedAxis.z * k) + crossedAxis.x,
		(crossedAxis.z * crossedAxis.z * k) + cosA);

	return result;*/
	return  Matrix3::from(AxisAngle::toFrom(to, from));

}

Matrix3 Matrix3::toFrom(Vector3 to, Vector3 from)
{
	return toFrom(normalize(to), normalize(from)); //normalize vectors to call the versor version.
}

Matrix3 Matrix3::from(Quaternion q) //DONE from
{
	Matrix3 tempMatrix;

	tempMatrix.firstVers.x = 1.0 - 2.0 * (q.y * q.y) - 2.0 * (q.z * q.z);
	tempMatrix.firstVers.y = 2.0 * (q.x * q.y) - 2.0 * (q.z * q.w);
	tempMatrix.firstVers.z = 2.0 * (q.x * q.z) + 2.0 * (q.y * q.w);

	tempMatrix.secondVers.x = 2.0 * (q.x * q.y) + 2.0 * (q.z * q.w);
	tempMatrix.secondVers.y = 1.0 - 2.0 * (q.x * q.x) - 2.0 * (q.z * q.z);
	tempMatrix.secondVers.z = 2.0 * (q.y * q.z) - 2.0 * (q.x * q.w);

	tempMatrix.thirdVers.x = 2.0 * (q.x * q.z) - 2.0 * (q.y * q.w);
	tempMatrix.thirdVers.y = 2.0 * (q.y * q.z) + 2.0 * (q.x * q.w);
	tempMatrix.thirdVers.z = 1.0 - 2.0 * (q.x * q.x) - 2.0 * (q.y * q.y);

	return tempMatrix;
}

// conversions to this representation
//Matrix3 Matrix3::from(Quaternion m);// TODO Q2M
Matrix3 Matrix3::from(Euler e)
{
	Matrix3 rotZ = rotationZ(e.z);
	Matrix3 rotY = rotationY(e.y);
	Matrix3 rotX = rotationX(e.x);

	return rotZ * rotX * rotY; //no additional parenthesis as we can trust the ++ and the result will be the same.
}     // DONE E2M
Matrix3 Matrix3::from(AxisAngle e) //https://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToMatrix/
{
	// DONE A2M
	double c = cos(e.angle);
	double s = sin(e.angle);
	double t = 1.0 - c;

	Matrix3 m;

	m.firstVers.x = c + (e.axis.x * e.axis.x * t);
	m.secondVers.y = c + (e.axis.y * e.axis.y * t);
	m.thirdVers.z = c + (e.axis.z * e.axis.z * t);

	Scalar tmp_1 = e.axis.x * e.axis.y * t;
	Scalar tmp_2 = e.axis.z * s;
	m.firstVers.y = tmp_1 + tmp_2;
	m.secondVers.x = tmp_1 - tmp_2;

	tmp_1 = e.axis.x * e.axis.z * t;
	tmp_2 = e.axis.y * s;
	m.firstVers.z = tmp_1 - tmp_2;
	m.thirdVers.x = tmp_1 + tmp_2;

	tmp_1 = e.axis.y * e.axis.z * t;
	tmp_2 = e.axis.x * s;
	m.secondVers.z = tmp_1 + tmp_2;
	m.thirdVers.y = tmp_1 - tmp_2;

	return m;
}
//static Matrix3 from(AxisAngle e); 

// does this Matrix3 encode a rotation?
bool Matrix3::isRot() const
{
	bool isRotation = false;

	if ((det() - 1.0 < EPSILON) && cross(firstVers, secondVers).isEqual(thirdVers.asVector()) && cross(thirdVers, firstVers).isEqual(secondVers.asVector()) && cross(secondVers, thirdVers).isEqual(firstVers.asVector()))
	{
		isRotation = true;
	}

	return isRotation;
	// DONE M-isR
}

// return a rotation matrix around an axis
Matrix3 Matrix3::rotationX(Scalar angleDeg)
{
	Matrix3 rotateX;
	Scalar radian = angleDeg * (M_PI / 180.0);
	rotateX.secondVers.y = cos(radian);
	rotateX.secondVers.z = sin(radian);
	rotateX.thirdVers.y = -sin(radian);
	rotateX.thirdVers.z = cos(radian);
	return rotateX;

};   // DONE M-Rx
Matrix3 Matrix3::rotationY(Scalar angleDeg)
{
	Matrix3 rotateY;
	Scalar radian = angleDeg * (M_PI / 180.0);
	rotateY.firstVers.x = cos(radian);
	rotateY.firstVers.z = -sin(radian);
	rotateY.thirdVers.x = sin(radian);
	rotateY.thirdVers.z = cos(radian);
	return rotateY;

	// DONE M-Ry
}
Matrix3 Matrix3::rotationZ(Scalar angleDeg)
{
	Matrix3 rotateZ;
	Scalar radian = angleDeg * (M_PI / 180.0);
	rotateZ.firstVers.x = cos(radian);
	rotateZ.firstVers.y = sin(radian);
	rotateZ.secondVers.x = -sin(radian);
	rotateZ.secondVers.y = cos(radian);
	return rotateZ;
	// DONE M-Rz
};

void Matrix3::printf() const
{
	std::cout << "[MATRIX]: \n";
	firstVers.printf();
	secondVers.printf();
	thirdVers.printf();
} // DONE

Scalar Matrix3::det() const
{
	return dot(cross(firstVers, secondVers), thirdVers.asVector());
}
Matrix3 Matrix3::inverse() const
{
	// DONE M-Inv a
	return transposed();
}

void Matrix3::invert()
{
	transpose();
	// DONE M-Inv b
}

// specific methods for Eulers...
Matrix3 Matrix3::transposed() const
{
	// DONE E-Transp a
	Matrix3 temp(firstVers, secondVers, thirdVers);
	temp.transpose();
	return temp;
}

void Matrix3::transpose()
{
	std::swap(firstVers.y, secondVers.x);
	std::swap(firstVers.z, thirdVers.x);
	std::swap(secondVers.z, thirdVers.y);
	// DONE E-Transp b
}





