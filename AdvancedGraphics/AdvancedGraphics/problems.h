#pragma once
#define _USE_MATH_DEFINES
// problems.h:
// a small set of matematical problems

#include <iostream>
#include "quaternion.h"
#include "vector3.h"
#include "versor3.h"
#include "point3.h"
#include "matrix3.h"
#include "euler.h"
#include "axis_angle.h"
#include <math.h>

// POD -- Plain Old Data
struct Ray
{
	Point3 p;
	Vector3 v;
};

struct Sphere
{
	Point3 c;
	Scalar r;
};

struct Plane
{
	// DONE-plane: fill
	Vector3 width;
	Vector3 height;
	Point3 origin;
};

const Point3 NO_INTERSECTION(666, 666, 666);

Point3 intersect(const Ray& r, const Sphere& s)
{
	Scalar a = dot(r.v, r.v);
	Scalar b = 2 * dot(r.p - s.c, r.v);
	Scalar c = dot(r.p - s.c, r.p - s.c) - s.r * s.r;

	// euqation is: a*k^2 + b*k + c == 0

	Scalar delta = b * b - 4 * a * c;
	if (delta < 0) return NO_INTERSECTION;

	Scalar k = (-b - sqrt(delta)) / (2 * a);
	if (k < 0) return NO_INTERSECTION;
	return r.p + k * r.v;
}

Point3 intersect(const Ray& r, const Plane& p) //as seen on lesson by professor
{
	// DONE: P-inter-plane
	Vector3 normalPlane = cross(p.width, p.height);
	Scalar discriminant = dot(r.v, normalPlane);
	Scalar RayDist = (dot(-(r.p - p.origin), normalPlane)) / discriminant;
	if (abs(discriminant) > EPSILON)
	{
		return r.p + (r.v * RayDist);
	}
	else
	{
		return NO_INTERSECTION;
	}
}

// does a eye in a given position sees the target, given the angle cone and maxDist?
bool isSeen(const Versor3& ViewDir, const Point3& eye, const Point3& target, Scalar angleDeg, Scalar maxDist)
{
	// DONE: isSeen
	Vector3 direction = target - eye;
	if (squaredNorm(direction) > (maxDist * maxDist))
	{
		return false; //out of  range
	}
	angleDeg *= M_PI / 180;
	Scalar viewDevided = cos(angleDeg / 2.0);
	return dot(normalize(direction), ViewDir) > viewDevided;
	//Check range
}

// returns the reflected direction of something bouncing in n
Versor3 reflect(const Versor3& d, const Versor3& n) // https://math.stackexchange.com/questions/13261/how-to-get-a-reflection-vector
{
	// DONE: P-reflect

	normalize(n);

	Versor3 temp = (d.asVector() - 2 * (dot(d, n) * n)).asVersor();
	return Versor3::up();
}

bool areCoplanar(const Versor3& a, const Versor3& b, const Versor3& c) //https://onlinemschool.com/math/library/vector/multiply2/
{
	Matrix3 mat = Matrix3(a, b, c);
	if (mat.det() == 0)
	{
		return true;
	}

	//another solution for it:
	//return dot(a, cross(b, c)) <= EPSILON; //SCALAR TRIPLE PRODUCT
	//DONE: P-coplanar
}

// normal of a plane, given three points
Versor3 planeNormal(const Point3& a, const Point3& b, const Point3& c)
{
	// DONE: P-planeNorm
	Vector3 aTOb = b - a;
	Vector3 aTOc = c - a;
	Vector3 crossProd = cross(aTOb, aTOc);

	return normalize(crossProd);
}

// return a versor as similar as possible to a, but ortogonal to b
Versor3 orthogonalize(const Versor3& a, const Versor3& b)
{
	// TODO: P-ortogonalize
	return Versor3::up();
}

// a bullet is in position pa and has velocity va
// a target is in position pb and has velocity vb
// returns the position of the target in the moment it is closest to the bullet
Point3 hitPos(const Point3& pa, const Vector3& va, const Point3& pb, const Vector3& vb)
{
	// TODO: P-hitPos
	return Point3();
}



