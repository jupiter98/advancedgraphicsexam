#pragma once
#include "versor3.h"
/* Quaternion class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all expected methods    */

class Matrix3;
class AxisAngle;
class Euler;
class Point3;
class Vector3;

typedef double Scalar;

class Quaternion
{
public:
	Quaternion(Scalar _x, Scalar _y, Scalar _z, Scalar _w);

	/* fields */
	// DONE Q-Fields: which fields to store? (also add a constructor taking these fields).
	Scalar x, y, z, w;

	Quaternion(Scalar _x, Scalar _y, Scalar _z);

	// DONE Q-Ide: this constructor constructs the identity rotation
	Quaternion();

	// DONE Q-FromPoint
	// returns a quaternion encoding a point
	Quaternion(const Point3& p);
	Quaternion(const Versor3& v);
	Quaternion(const Vector3& v);

	Vector3 apply(Vector3  v) const;

	// Rotations can be applied to versors or vectors just as well DONE
	Versor3 apply(Versor3 dir) const;

	Point3 apply(Point3 p) const;

	// syntactic sugar: "R( p )" as a synonym of "R.apply( p )"
	Versor3 operator() (Versor3 p);
	Point3  operator() (Point3  p);
	Vector3 operator() (Vector3 p);

	Versor3 axisX() const;  // DONE Q-Ax a
	Versor3 axisY() const;  // DONE Q-Ax b
	Versor3 axisZ() const;  // DONE Q-Ax c

	Quaternion operator* (Quaternion r) const;

	Quaternion operator*(Scalar k)const;

	// Quaternion sum
	Quaternion operator+(const Quaternion& b) const;

	Quaternion operator-() const;


	Quaternion inverse() const;

	void invert();

	// specific methods for quaternions...
	Quaternion conjugated() const;

	void conjugate();

	// returns a rotation to look toward target, if you are in eye, and the up-vector is up
	static Quaternion lookAt(Point3 eye, Point3 target, Versor3 up = Versor3::up());

	// returns a rotation
	static Quaternion toFrom(Versor3 to, Versor3 from);

	static Quaternion toFrom(Vector3 to, Vector3 from);

	// conversions to this representation
	static Quaternion from(const Matrix3& m);
	static Quaternion from(const Euler& e);
	static Quaternion from(const AxisAngle& e);

	// does this quaternion encode a rotation?
	bool isRot() const;


	// does this quaternion encode a point?
	bool isPoint()const;

	void printf() const;

	static Quaternion rotationX(Scalar angleDeg);
	static Quaternion rotationY(Scalar angleDeg);
	static Quaternion rotationZ(Scalar angleDeg);
};

inline Scalar dot(const Quaternion& a, const Quaternion& b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}

inline void normalize(Quaternion p)
{
	Scalar n = sqrt(p.x * p.x + p.y * p.y + p.z * p.z + p.w * p.w);
	p.x /= n;
	p.y /= n;
	p.z /= n;
	p.w /= n;
}

// interpolation or rotations
inline Quaternion lerp(const Quaternion& a, const Quaternion& b, Scalar t)
{

	Quaternion temp = a * (1 - t) + ((dot(a, b) > 0) ? b : -b) * t; //from Mathematics for 3D game programming + lesson FIX
	normalize(temp);
	return temp;
	// DONE Q-Lerp: how to interpolate quaternions
	// hints: shortest path! Also, consider them are 4D unit vectors.
}

// interpolation or rotations
inline Quaternion sLerp(const Quaternion& a, const Quaternion& b, Scalar t)
{
	Scalar angle = acos(dot(a, b));
	Quaternion r = (a * ((sin(angle * (1 - t)) / sin(angle))) + (b * ((sin(angle) * t) / sin(angle)))); //from Mathematics for 3D game programming
	return r;
	// DONE Q-Slerp: how to interpolate quaternions
	// hints: shortest path! Also, consider them are 4D unit vectors.
}



