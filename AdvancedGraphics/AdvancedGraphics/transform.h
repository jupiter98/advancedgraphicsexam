#pragma once


class Vector3;
class Versor3;
class Point3;
typedef double Scalar;
// The BIG CHOICE:
typedef Quaternion Rotation;
//typedef AxisAngle Rotation;
//typedef Matrix3 Rotation;
//typedef Euler Rotation;

class Transform
{
public:
	Scalar   scale;   // scaling (isotropic)
	Rotation rotation; // rotation
	Vector3  translation;  // translation

	// description of the local frame!
	Point3 origin() const;
	Versor3 left() const;
	Versor3 right()const;
	Versor3 up() const;
	Versor3 down()const;
	Versor3 forward() const;
	Versor3 backward()const;

	// constructor: is the ide
	Transform();

	Versor3 apply(Versor3 dir);
	Vector3 apply(Vector3 v);

	Point3 apply(Point3 p);
	// inverse out-of-place
	Transform inverse() const;


	void invert();

	// CUMULATE: first b, then *this
	Transform operator * (const Transform& b);

	// fills a column-major Scalar 4x4 zmatrix for DirectX (or OpenGL)
	// vector d is already allocated
	void fillDirectXMatrix(Scalar d[]);

	// places this transform in the given origin, looking (Z-axis) toward target, given the up vector
	void place(Point3 origin, Point3 target, Versor3 up);

	void printf() const;

};

Transform lerp(const Transform& a, const Transform& b, float t);

