
#pragma once 

#include <math.h>
/* Euler class */
/* this class is a candidate to store a rotation! */
/* as such, it implements all the expected methods    */
typedef double Scalar;

class Quaternion;
class AxisAngle;
class Matrix3;
class Euler
{
public:

	/* fields */
	// DONE E-Fields: which fields to store? (also add a constructor taking these fields).
	Scalar x; //PITCH
	Scalar y; //YAW
	Scalar z; //ROLL 

	// DONE E-Ide: this constructor construct the identity rotation
	Euler();

	Euler(Scalar m00, Scalar m01, Scalar m02);

	Vector3 apply(Vector3  v) const;

	// Rotations can be applied to versors or vectors just as well
	Versor3 apply(Versor3 dir) const;

	Point3 apply(Point3 p) const;

	// syntactic sugar: "R( p )" as a synomim of "R.apply( p )"
	Versor3 operator() (Versor3 p);
	Point3  operator() (Point3  p);
	Vector3 operator() (Vector3 p);

	Versor3 axisX() const;
	Versor3 axisY() const;
	Versor3 axisZ() const;

	// conjugate
	Euler operator* (Euler b) const;

	Euler inverse() const;

	void invert();

	// returns a rotation to look toward target, if you are in eye, and the up-vector is up
	static Euler lookAt(Point3 eye, Point3 target, Versor3 up = Versor3::up());
	// returns a rotation
	static Euler toFrom(Versor3 to, Versor3 from);

	static Euler toFrom(Vector3 to, Vector3 from);
	// conversions to this representation
	static Euler from(Quaternion q);// DONE Q2M
	static Euler from(Matrix3 m);   // TODO E2M
	static Euler from(AxisAngle e); // TODO E2M

	// does this Euler encode a rotation?
	bool isRot() const;

	// return a rotation matrix around an axis
	static Euler rotationX(Scalar angleDeg);
	static Euler rotationY(Scalar angleDeg);
	static Euler rotationZ(Scalar angleDeg);

	void printf() const;
};


// interpolation or rotations
inline Euler directLerp(const Euler& a, const Euler& b, Scalar t)
{
	// TODO E-directLerp: how to interpolate Eulers
	return Euler();
}

inline Euler lerp(const Euler& a, const Euler& b, Scalar t)
{
	// TODO E-smartLerp: how to interpolate Eulers
	return Euler();
}


