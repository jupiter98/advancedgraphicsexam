#pragma once
#include <math.h>
#include <assert.h>
#include "vector3.h"

typedef double Scalar;

class Versor3
{
	Versor3(Scalar _x, Scalar _y, Scalar _z);
public:
	// constructors
	Scalar x, y, z;

	static Versor3 right();
	static Versor3 left();
	static Versor3 up();
	static Versor3 down();
	static Versor3 forward();
	static Versor3 backward();

	//access to the coordinates: to write them
	Scalar& operator[] (int i);

	//access to the coordinates: to read them
	Scalar operator[] (int i) const;

	Vector3 operator*(Scalar k) const;

	Versor3 operator -() const;
	Versor3 operator +() const;
	friend Versor3 normalize(Vector3 p);
	friend Versor3 Vector3::asVersor() const;

	Vector3 asVector() const;

	void printf() const;

};

inline Versor3 normalize(Vector3 p)
{
	Scalar normal = norm(p);
	if (normal <= (0 + EPSILON)) return p.asVersor();
	else
	{
		return Versor3(p.x / normal, p.y / normal, p.z / normal);
	}
}

// cosine between a and b
inline Scalar dot(const Versor3& a, const Versor3& b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

// length of projection of b along a
inline Scalar dot(const Versor3& a, const Vector3& b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

inline Vector3 cross(const Versor3& a, const Versor3& b)
{
	return Vector3(
		a.y * b.z - a.z * b.y,
		a.z * b.x - a.x * b.z,
		a.x * b.y - a.y * b.x);
}

inline Vector3 operator*(Scalar k, const Versor3& a) { return a * k; }

inline Versor3 nlerp(const Versor3& a, const Versor3& b, Scalar t)
{
	// DONE nlerp
	return normalize((1 - t) * a + t * b);
}

inline Versor3 slerp(const Versor3& a, const Versor3& b, Scalar t)
{
	// DONE slerp
	Scalar cosAngle = dot(a, b);
	Scalar angle = acos(cosAngle);
	Vector3 vectorA = (sin((1 - t) * angle) / sin(angle) * a);
	Vector3 vectorB = ((sin(t * angle) / sin(angle)) * b);
	return (vectorA + vectorB).asVersor();
}

// under my own responsibility, I declare this vector to be unitary and therefore a VERSOR
inline Versor3 Vector3::asVersor() const
{
	// DONE: a nice assert?
	Vector3 vector = Vector3(x, y, z);
	assert((norm(vector) - 1 <= EPSILON2));
	//normalized versor
	return Versor3(x, y, z);
}


