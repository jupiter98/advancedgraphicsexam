Notes:

The classes Quaternion, AxisAngle, Matrix3, Euler
are alternative ways to represent a rotation.

They expose many of the same methods (apply, inverse, lerp...).

It must be possible to use them interchangably (search source for "the BIG CHOICE").

There are "from" methods that convert one type to the next.

Your task is to complete the "TODO" methods.

* Keep track of the one you did in a done.txt file! Each TODO has a label. 
  E.g. if you implement "TODO Q-invert"
  then write down: "Q-Invert: DONE"

* Many methods can be completed my implementing the "from" method.
  E.g., to invert Axis-angles, convert them to matrix3, invert matrix3, convert back...
  (This includes CONVERSION METHODS!)
  If you do this, keep track. E.g. "Q-invert (via Matrix3)"

* Classes that interpolate badly (Matrix, Euler) have two interpolation methods;
  a directLerp, which intepolates badly,
  a lerp, which inerpolates nicely (convert to another format, interpolate, convert back)

In addition, file "problems.h" has simple problems with their own UNDO.

Please write more unittest to check that your methods are correct!

Ignore "matrix4.h". It is a rendundant file (for now).






RESULTS:

ALL printf() DONE

VERSOR3:
 Nlerp
 Slerp
 asVersor

VECTOR3:
 All done.

QUATERNION:
Q-Constr
Q-Ide
Q-FromPoint
Q-App
Q-InvA
Q-InvB
Conj-A
Q-Conjb
Q-LookAt
Q-ToFrom
M2Q
E2Q
A2Q
Q-isR
Q-Rx-
Q-Ry
Q-Rz
Q-Fields
Q-Ide
Q-Ax a 
Q-Ax b
Q-Ax c
Q-Lerp
Q-Slerp

TRANSFORM:
T-invert
T-place
T-invert(outofplace)
T-Lerp

POINT:
All Done

MATRIX:
M-Fields
M-Constr
M-Ide
M-Ax a
M-Ax b
M-Ax c
Operator * 
M-LookAt
M-ToFrom
Q2M
E2M
A2M
M2E
M-isR
M-Rx
M-Ry
M-Rz
M-Inv a
M-Inv b
M-Transp a
M-Transp b

EULER:
E-Fields
E-Ide
E-Constr
E-Ax a
E-Ax b
E-Ax c
E-inv b
E-Rx
E-Ry
E-Rz
E-inv

AXIS ANGLE:
A-Field
A-Ide
A-FromPoint
A-Ax a
A-Ax b
A-Ax c
M2A
A-Lerp
A-Ide
A-Inv a 
A-Inv b
A-LookAt
A-ToFrom
M2A
E2A
Q2A
A-isP
A-Rx
A-Ry
A-Rz

PROBLEMS:
plane-fill
P-inter-plane
isSeen
P-reflect
P-coplanar
P-planeNorm
